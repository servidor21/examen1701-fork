## Examen Laravel. Febrero 2017

### Enunciado
- Desarrollar un proyecto sobre recetas de cocina. Con las siguientes tablas:
    1. User (ya definida por Laravel).
    2. Type. Tipo de platos
    3. Ingredient.
    4. Dish. Platos o recetas recogidas en la aplicación. Cada plato es de un tipo determinado, pertenece a un usuario y consta de varios ingredientes (y su cantidad).
- Clonar este repositorio localmente. En él ya cuentas con:
    - La estructura base de Laravel
    - Migración de las tablas y seeders.
    - FALTA: modelos y métodos que reflejen las relaciones de clave ajenta entre ellas.

### Ejercicios
1. Configura Laravel para comenzar a funcionar. Crea una base de datos examen1701.
2. Añade los modelos necesarios y sus métodos de relaciones para que las migraciones y seeders funcionen correctamente.  (1Punto)
3. Crea un controlador _resource_ para gestionar la tabla de platos (dishes) y la ruta o rutas necesarias.
4. **Index**. Listado de platos. Debe incluír el nombre del usuario y del tipo de plato. (1 Punto)
5. **create/store**. Alta de platos, sin ingredientes, pero con usuario y tipo.  (1 Punto).
6. **Validación**  del alta de platos con mensajes en español. (1 Punto).
7. **show**. Muestra del detalle de un plato. Debe incluír la lista de ingredientes con su cantidad. Acceso público (1 Punto).
8. **delete**. Borrado de platos. (1 Punto).
9. **Autorización** (2 Puntos). 
    - INDEX/SHOW. Listado y vista de detalle. Acceso público.
    - CREATE/STORE. Creación de platos. Acceso para todos los usuarios logueados. Además el enlace de nuevo en la lista de platos debe mostrarse/ocultarse según corresponda.
    - DELETE. Borrado de platos. Sólo puede hacerlo el usuario propietario. El botón de borrado sólo debe aparecer para los platos del usuario actual.
11. **API REST** básica del modelo Ingredient. Crea el controlador correspondiente y la ruta o rutas necesarias en api.php. Debe contener los métodos index, show, store, delete y update.  (2 Puntos).