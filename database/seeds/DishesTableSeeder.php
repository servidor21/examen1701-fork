<?php

use Illuminate\Database\Seeder;

class DishesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        for ($i=1; $i < 100; $i++) {
            $dish = factory(App\Dish::class)->create();

            $ingredients = App\Ingredient::all()->random(rand(4, 8));
            foreach ($ingredients as $ingredient) {
                $id = $ingredient->id;
                $quantity = rand(1, 50);
                $dish->ingredients()->attach($id, [
                    'quantity' => $quantity
                    ]);
            }
        }
    }
}
